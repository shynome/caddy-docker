
#
# Builder
#
FROM golang:1.11.5-alpine3.8 as builder

RUN apk add --no-cache git gcc musl-dev

RUN go get -v github.com/abiosoft/parent

COPY builder /builder

ARG version="0.11.4"
ARG plugins="git,cors,realip,expires,cache,ipfilter"

RUN VERSION=${version} PLUGINS=${plugins} /bin/sh /builder/init.sh
RUN plugin_alidns='github.com/shynome/caddy-dnsproviders/alidns' \
    && go get -v $plugin_alidns \
    && wget -O /go/src/github.com/mholt/caddy/caddyhttp/alidns.go https://raw.githubusercontent.com/shynome/caddy-dnsproviders/master/caddy-plugins-import-0.11.4
RUN VERSION=${version} PLUGINS=${plugins} /bin/sh /builder/build.sh

#
# Final stage
#
FROM alpine:3.8

ARG version="0.11.4"
LABEL caddy_version="$version"

# Let's Encrypt Agreement
ENV ACME_AGREE="false"

# Telemetry Stats
ENV ENABLE_TELEMETRY="false"

RUN apk add --no-cache tzdata openssh-client curl

# install process wrapper
COPY --from=builder /go/bin/parent /bin/parent

# install caddy
COPY --from=builder /install/caddy /usr/bin/caddy

# validate install
RUN /usr/bin/caddy -version
RUN /usr/bin/caddy -plugins

EXPOSE 80 443 2015
VOLUME /root/.caddy /srv
WORKDIR /srv

COPY rootfs /

ENTRYPOINT ["/bin/parent", "caddy"]
CMD ["--conf", "/etc/Caddyfile", "--log", "stdout", "--agree=$ACME_AGREE"]

