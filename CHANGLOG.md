# CHANGELOG

## 1.2.2

### ADD

- `tls.dns` 同时支持 `alidns` 和 `alicloud` 这两个名字

### REMOVE

- 不再更改默认 DNS服务器, 而是从环境变量: `RECURSIVE_NAMESERVERS` 中获取

## 1.2.0

### ADD

- 添加了 `tls.dns.alicloud` 插件, 用以支持阿里云的dns验证

### BREAK

- 默认 DNS 服务器由 谷歌DNS服务器 变更为 `dns21.hichina.com,dns22.hichina.com`

## 1.1.3

### ADD

- 添加了 `ipfilter` 插件

### REMOVE

- 移除了 `filemanager` 插件

## 1.1.2

### FIX

- `caddy` 只需要 `openssh-client` 和 `curl` 这两个包

## 1.1.1

### FIX

- 因为缺少 `abiosoft/caddy` 中的某些环境变量, 导致证书申请不下来的问题

## 1.1.0

### CHANG

- 去除 `abiosoft/caddy` 镜像中有关 `php` 的功能

## 1.0.1

### ADD

- 添加了 `apk add --no-cache tzdata` 可以使用环境变量来设置时区了